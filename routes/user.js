const { Router, Router } = require('express');
const { usuarioGet } = require('../../controllers/user');

const router = Router();
router.put('/:id', usuarioPut);

//router.get('/', usuarioGet);
/* router.get('/', (req, res) => {
    res.json({
      msg: 'get API'
    });
  }); */
  
  router.put('/', (req, res) => {
    res.json({
      msg: 'put API'
    });
  });
  
  router.post('/', (req, res) => {
    res.status(201).json({
      msg: 'post API'
    });
  });

  router.put('/:id', usuarioPut);

  router.delete('/', (req, res) => {
    res.json({
      msg: 'delete API'
    });
  });
module.exports=router